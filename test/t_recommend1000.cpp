/*************************************************************************
    @ File Name: t_recommendEngine.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年07月20日 星期一 15时10分54秒
 ************************************************************************/
#include "../recommendEngine.h"

using namespace std;

int main(int argc, char* argv[])
{
	/*
	if(argc < 2)
	
	{
		cout << "Usage: Please input query and  rerun the program!" << endl;
		return -1;
	}*/

	cout << "Start recommend engine ..." << endl;
	string pth = "/work/mproj/workproj/kuaipan/query_recomm_http/q_similar/dict";
	string dict = "/work/mproj/workproj/kuaipan/recommendation/recommend/resource/";
	size_t st = time(NULL);
	recommendEngine mRecomm(pth, dict);
    //string s = "我的POLO裙子";
	//string s = argv[1];
	string s = "";
	string res;
	cout << "time cost:" <<(float)(time(NULL) - st)/60 << endl;
	cout << "please input query:" << endl;
	ifstream fin;
	fin.open("q1000.txt");
	if(!fin.is_open())
		cerr << "Open test file error!\n";

	while(getline(fin,s))
	{
		res = "";
	mRecomm.jsonResults(s,res);
	cout << "recommend results:" << res << endl;
	}
	cout << "End process!\t";	

	fin.close();
	return 0;
}


