/*************************************************************************
    @ File Name: t_queryPure.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年08月05日 星期三 15时15分58秒
 ************************************************************************/
#include <iostream>
#include <vector>
#include <fstream>

#include<boost/unordered_map.hpp>
#include<boost/algorithm/string.hpp>
#include<boost/algorithm/string/replace.hpp>
#include "knlp/horse_tokenize.h"
#include "../normalize.h"

using namespace std;

int main()
{
	ilplib::knlp::HorseTokenize* tok_;
	string tok_pth = "/work/mproj/workproj/kuaipan/recommendation/recommend/dict";

	//initialize tokenzier
	tok_ = new ilplib::knlp::HorseTokenize(tok_pth);

	string strFile = "/work/mproj/workproj/kuaipan/dictionary/querylog/keywords-six.all";
//	string strFile = "/work/mproj/workproj/kuaipan/dictionary/querylog/keyword_20141201_20150131_corpus_results.corp";
//	string strFile = "/work/mproj/workproj/kuaipan/dictionary/querylog/keyword_20150201_20150331_corpus_results.corpls";
//	string strFile = "/work/mproj/workproj/kuaipan/dictionary/querylog/201504-05_search_corpus_results.corp";

	ifstream inFile;
	inFile.open(strFile.c_str());
	if(!inFile)
		cerr << "Open process file error !\n";

	string sLine = "";
	string sstr = "";
	boost::unordered_map<string,uint32_t> terms;
	boost::unordered_map<string,uint32_t>::iterator it;

	vector<pair<string,float> > segTerms;
	vector<string> context;
	vector<string> query;

	terms.clear();
	segTerms.clear();
	context.clear();

	//process every query
	while(getline(inFile,sLine))
	{
		if(1 > sLine.length())
			continue;
		context.clear();
		terms.clear();

		boost::split(context,sLine,boost::is_any_of("\t"));
		if( 4 > context.size())
			continue;
		
		//seg
		try
		{
			tok_->tokenize(context[0],segTerms);
		}
		catch(...)
		{
			segTerms.clear();
		}
		
		ilplib::knlp::Normalize::normalize(context[0]);
		
		//dedup
		for(unsigned int i = 0; i < segTerms.size();++i)
		{
			cout << "Terms:" << segTerms[i].first << ",";
			it = terms.find(segTerms[i].first);
			if(it != terms.end())
			{
				boost::algorithm::replace_first(context[0],it->first," ");
				ilplib::knlp::Normalize::normalize(context[0]);
				continue;
			}
			else
				terms.insert(make_pair(segTerms[i].first,1));
		}
		cout << "\t query:" << context[0] << endl;
		sstr = "";
		//
		sstr += context[0];
		sstr += "\t";
		sstr += context[1];
		sstr += "\t";
		sstr += context[2];
		sstr += "\t";
		sstr += context[3];
		cout << "after: " << context[0] << endl;		
		query.push_back(sstr);	
	}


	//output 
	ofstream outFile;
	string outpth = "/work/mproj/workproj/kuaipan/dictionary/querylog/queryPure.all"; 	

	outFile.open(outpth.c_str(),ios::app);
	if(!outFile)
		cerr << "open output file error!\n";
	for(unsigned int j = 0;j < query.size();++j)
		outFile << query[j] << '\n';

	outFile.close();
	inFile.close();
	return 0;
}


