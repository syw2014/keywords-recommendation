/*************************************************************************
    @ File Name: t_dedupQuery.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年08月04日 星期二 09时33分40秒
 ************************************************************************/
#include <iostream>
#include <vector>

#include <boost/unordered_map.hpp>
#include <boost/algorithm/string/replace.hpp>
#include "knlp/horse_tokenize.h"
#include "../normalize.h"


using namespace std;


int main()
{
	ilplib::knlp::HorseTokenize* tok_;
string dir = "/work/mproj/workproj/kuaipan/query_recomm_http/q_similar/dict";
	tok_ = new ilplib::knlp::HorseTokenize(dir);

	boost::unordered_map<string,uint32_t> terms;
	boost::unordered_map<string,uint32_t>::iterator it;

	vector<pair<string,float> > segTerms;
	vector<string> query;
	vector<string> afquery;

	string str = "";

	terms.clear();
	query.clear();

	query.push_back("冰箱 冰箱 大家电 冰箱");
	query.push_back("【16寸变速折叠单车碟刹减震成人男女式自行车");
	query.push_back("帝度冰箱 冰箱 大家电");
	query.push_back("撞色連衣裙 夏季");	

	for(unsigned int i = 0; i < query.size(); ++i)
	{
		cout << "Befor seg:\t" << query[i] << '\n';
		segTerms.clear();
		terms.clear();
		try
		{
			tok_->tokenize(query[i],segTerms);
		}
		catch(...)
		{
			segTerms.clear();
		}
	
		str = query[i];
		
		ilplib::knlp::Normalize::normalize(str);
		cout << "after:" << str << endl;
		//dedup
		for(unsigned int j = 0;j < segTerms.size();++j)
		{
			cout << "terms:" << segTerms[i].first << endl;
			it = terms.find(segTerms[j].first);
			if(it != terms.end())
			{
				ilplib::knlp::Normalize::normalize(str);
				boost::algorithm::replace_first(str, it->first ," ");
				ilplib::knlp::Normalize::normalize(str);
				continue;
			}
			else
			{
				terms.insert(make_pair(segTerms[j].first,11));
				//str += segTerms[j].first + " ";
			}
		}
		cout << "After seg:\t" << str << '\n';
		afquery.push_back(str);
	}


	delete tok_;

	return 0;

}
