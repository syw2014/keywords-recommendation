/************************************************************************
    @ File Name: t_tokenizer.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年08月06日 星期四 16时27分06秒
 ************************************************************************/
#include <iostream>
#include <vector>

#include "horse_tokenize.h"

using namespace std;


int main()
{
	ilplib::knlp::HorseTokenize* tok_;
	string tok_pth = "/work/mproj/workproj/kuaipan/query_recomm_http/q_similar/dict";
	tok_ = new ilplib::knlp::HorseTokenize(tok_pth);

	vector<pair<string,float> > segTerms;

	string ss = "冰箱 冰箱 大家电 手機";
	
	try
	{
		tok_->tokenize(ss,segTerms);
	}
	catch(...)
	{
		segTerms.clear();
	}
		
	for(unsigned int i = 0; i < segTerms.size();++i)
		cout << "Terms:" << segTerms[i].first << "\tWeight:" << segTerms[i].second<<endl;


	delete tok_;
}
