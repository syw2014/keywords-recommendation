/*************************************************************************
    @ File Name: t_trd2simp.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年08月11日 星期二 10时02分12秒
 ************************************************************************/
#include <iostream>

#include "trd2simp.h"
#include "kstring.hpp"

using namespace std;

int main()
{
	Trad2Simp trd2simp_;
	string s = "手機";
	KString ss(s);
//	ss.to_dbc();
//	ss.to_lower_case();
	trd2simp_.transform(ss);
//	ss.trim_into_1();
//	ss.trim_head_tail();
	
	cout << "after:" << ss << endl;

	return 0;
}
